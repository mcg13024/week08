package Week08redo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class doingSomething {

    public static void main(String[] args){

        ExecutorService myService = Executors.newFixedThreadPool(3);
        doSomething ds1 = new doSomething("Fred", 23,1000);
        doSomething ds2 = new doSomething("Tom", 14,500);
        doSomething ds3 = new doSomething("Luke", 7,250);
        doSomething ds4 = new doSomething("Anna", 5,100);
        doSomething ds5 = new doSomething("Sam", 3,50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
